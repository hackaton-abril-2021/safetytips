package com.techu.safety.controller.security;

import com.techu.safety.model.SafetyTipModel;
import com.techu.safety.service.SafetyTipMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/safety/")
public class SafetyControllerSecurity {

    @Autowired
    SafetyTipMongoService safetyTipMongoService;

    /* Get lista de productos */
    @GetMapping("/tips")
    public ResponseEntity<List<SafetyTipModel>> getSafetyTips() {
        return new ResponseEntity<>(safetyTipMongoService.findAll(), HttpStatus.OK);
    }

    /*Add nuevo producto*/
    @PostMapping(value = "/tips")
    public ResponseEntity<String> postSafetyTip(@RequestBody SafetyTipModel safetyTipToCreate) {
        safetyTipMongoService.save(safetyTipToCreate);
        return new ResponseEntity<String>("SafetyTip creado correctamente", HttpStatus.CREATED);
    }

    /* PUT actualizar producto */
    @PutMapping("/tips")
    public ResponseEntity<SafetyTipModel> putProductos(@RequestBody SafetyTipModel safetyTipToUpdate){
        SafetyTipModel safetyTipUpdated = safetyTipMongoService.save(safetyTipToUpdate);
        return new ResponseEntity<>(safetyTipUpdated, HttpStatus.OK);
    }

    @DeleteMapping("/tips")
    public ResponseEntity<String> deleteProductos(@RequestBody SafetyTipModel productoToDelete){
        if(safetyTipMongoService.delete(productoToDelete))
            return new ResponseEntity<>("Producto eliminado",HttpStatus.OK);
        else
            return new ResponseEntity<>("Id de Producto no informado",HttpStatus.BAD_REQUEST);
    }
}