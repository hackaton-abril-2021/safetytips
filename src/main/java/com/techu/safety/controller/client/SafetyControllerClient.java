package com.techu.safety.controller.client;


import com.techu.safety.model.SafetyTipModel;
import com.techu.safety.service.SafetyTipMongoService;
import com.techu.safety.service.SafetyTipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController()
@RequestMapping("/api/v0/safety/")
public class SafetyControllerClient {

    @Autowired
    private SafetyTipService safetyTipService;


    /* Get lista de tips */
    @GetMapping(value = "/tips")
    public ResponseEntity<List<SafetyTipModel>> obtenerListado() {
        List<SafetyTipModel> listadotips = safetyTipService.getSafetyTips();
        return new ResponseEntity<>(listadotips, HttpStatus.OK);
    }

    /* Get tip por Random */
    @GetMapping("/tips/random")
    public ResponseEntity<SafetyTipModel> getTipsRandom() {
        if(safetyTipService.findById() != null)
             return new ResponseEntity<>(safetyTipService.findById(), HttpStatus.OK);
         else
             return new ResponseEntity<>(safetyTipService.findById(), HttpStatus.NOT_FOUND);
    }

}