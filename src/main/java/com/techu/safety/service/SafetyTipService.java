package com.techu.safety.service;

import com.techu.safety.model.CodeQRModel;
import com.techu.safety.model.ProductoModel;
import com.techu.safety.model.SafetyTipModel;
import com.techu.safety.repository.SafetyTipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SafetyTipService {

    @Autowired
    CodeQuickResponseService codeQuickResponseService;

    @Autowired
    SafetyTipRepository safetyTipRepository;

    private List<SafetyTipModel> dataList;


    private List<ProductoModel> productos;


    private List<CodeQRModel> codigosQr;


    // READ Collections
    public List<SafetyTipModel> getSafetyTips() {
        return safetyTipRepository.findAll();
    }

    // READ Collections id
    public SafetyTipModel findById() {
        SafetyTipModel safetyTipModel = null;
        int idAleatory = 0;
        int size = safetyTipRepository.findAll().size();
        /*int size = safetyTipService.getSafetyTips().size() - 1;*/

        if (size > 0) {

            idAleatory = (int) (Math.random() * size);
            safetyTipModel = getSafetyTips().get(idAleatory);

            String QRCode = codeQuickResponseService.getCodeQuickResponseService(safetyTipModel.getQrCode().getContent());
            safetyTipModel.setQrCodes(new CodeQRModel(QRCode));

        }
        return safetyTipModel;
    }


}
