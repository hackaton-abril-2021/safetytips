package com.techu.safety.service;

import com.techu.safety.model.CodeQRModel;
import com.techu.safety.model.codequickresponse.CodeQRRequestModel;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

@Service
public class CodeQuickResponseService {

    final static String END_POINT_QR_CODE = "https://neutrinoapi.net/qr-code";

    public String getCodeQuickResponseService(String content) {
        RestTemplate template = new RestTemplate();
        byte[] imageCQR = template.postForObject(END_POINT_QR_CODE, new CodeQRRequestModel(content), byte[].class);


        return new String(imageCQR, StandardCharsets.UTF_8);
    }

}
