package com.techu.safety.service;

import com.techu.safety.model.SafetyTipModel;
import com.techu.safety.repository.SafetyTipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SafetyTipMongoService {

    @Autowired
    SafetyTipRepository safetyTipRepository;

    public List<SafetyTipModel> findAll() {
        return safetyTipRepository.findAll();
    }

    public SafetyTipModel save(SafetyTipModel entity) {
        return safetyTipRepository.save(entity);
    }

    public boolean delete(SafetyTipModel entity) {
        try {
            safetyTipRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
