package com.techu.safety.repository;

import com.techu.safety.model.SafetyTipModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SafetyTipRepository extends MongoRepository<SafetyTipModel, String> {
    List<SafetyTipModel> findAll();

    SafetyTipModel save(SafetyTipModel entity);

    void delete(SafetyTipModel entity);
}
