package com.techu.safety.model;

import java.io.Serializable;

public class CodeQRModel implements Serializable {

    private String content;

    public CodeQRModel() {

    }

    public CodeQRModel(String content) {

        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "CodeQRModel{" +
                "content='" + content + '\'' +
                '}';
    }
}
