package com.techu.safety.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document("safetyTip")
public class SafetyTipModel  implements Serializable {

    private String id;
    private String descripcion;
    private String tipo;
    private List<ProductoModel> productos;
    private CodeQRModel qrCode;



    public SafetyTipModel(String id, String descripcion, String tipo, List<ProductoModel> productos, CodeQRModel qrCode) {

        this.id = id;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.productos = productos;
        this.qrCode = qrCode;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public CodeQRModel getQrCode() {
        return this.qrCode;
    }

    public void setQrCodes(CodeQRModel qrCode) {
        this.qrCode = qrCode;
    }

    public List<ProductoModel> getProductos() {
        return this.productos;
    }

    public void setProductos(List<ProductoModel> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "SafetyTipModel{" +
                "id='" + id + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", tipo='" + tipo + '\'' +
                ", productos=" + productos +
                ", qrCodes=" + qrCode +
                '}';
    }
}
