package com.techu.safety.model.codequickresponse;

import org.springframework.beans.factory.annotation.Value;

public class CodeQRRequestModel {

    private String content;
    private int width;
    private int height;
    private String fg_color;
    private String bg_color;
    private String user_id;
    private String api_key;

    public CodeQRRequestModel(String content) {
        this.content = content;
        this.width = 256;
        this.width = 256;
        this.fg_color = "#000000";
        this.bg_color = "#ffffff";
        this.user_id="santhenao2";
        this.api_key="GRx7GxNao3Y9H8J7CABrtF9VeWJF4WeieohFYjK0YDC0r9qq";
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getFg_color() {
        return fg_color;
    }

    public void setFg_color(String fg_color) {
        this.fg_color = fg_color;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    @Override
    public String toString() {
        return "GeneracionQR{" +
                "content='" + content + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", fg_color='" + fg_color + '\'' +
                ", bg_color='" + bg_color + '\'' +
                ", user_id='" + user_id + '\'' +
                ", api_key='" + api_key + '\'' +
                '}';
    }
}
