package com.techu.safety.model;


import java.io.Serializable;

public class ProductoModel implements Serializable {

    private String id;
    private String nombre;


    public ProductoModel(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public String toString() {
        return "ProductosModel{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
